import tkinter as tk
from tkinter import ttk
import sqlite3

class Main(tk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.pack()
        self.init_main()
        self.db = db
        self.view_records()

    def init_main(self):
        toolbar = tk.Frame(bg = '#d7d8e0', bd = 2)
        toolbar.pack(side = tk.TOP, fill = tk.X)

        self.add_img = tk.PhotoImage(file = 'img//add.gif')
        self.btn_open_dialog = tk.Button(toolbar, text = 'Add', command =self.open_dialog, bg = '#d7d8e0', bd = 0,
                                    compound=tk.TOP, image=self.add_img)
        self.btn_open_dialog.pack(side=tk.LEFT)

        self.tree = ttk.Treeview(self, columns = ('ID', 'desc', 'costs', 'total'), height=15, show='headings')

        self.tree.column('ID', width=30, anchor=tk.CENTER)
        self.tree.column('desc', width=365, anchor=tk.CENTER)
        self.tree.column('costs', width=150, anchor=tk.CENTER)
        self.tree.column('total', width=100, anchor=tk.CENTER)

        self.tree.heading('ID', text='ID')
        self.tree.heading('desc', text='Опис')
        self.tree.heading('costs', text='Кошти')
        self.tree.heading('total', text='Сума')

        self.tree.pack()

    def open_dialog(self):
        Child()

    def view_records(self):
        self.db.c.execute('''SELECT * FROM finance''')
        [self.tree.delete(i) for i in self.tree.get_children()]
        [self.tree.insert('', 'end', values = row) for row in self.db.c.fetchall()]

    def records(self, desc, costs, total):
        self.db.insert_data(desc, costs, total)
        self.view_records()

class Child(tk.Toplevel):
    def __init__(self):
        super().__init__(root)
        self.init_child()
        self.view = app

    def init_child(self):
        self.title('MoneyCounter')
        self.geometry('450x230+400+300')
        self.resizable(False, False)

        label_description = tk.Label(self, text='Назва')
        label_description.place(x = 50, y = 50)
        label_select = tk.Label(self, text='Стаття прибутків/витрат')
        label_select.place(x = 50, y = 80)
        label_sum = tk.Label(self, text='Сума:')
        label_sum.place(x = 50, y = 110)

        self.entry_description = ttk.Entry(self)
        self.entry_description.place(x = 200, y = 50)

        self.entry_money = ttk.Entry(self)
        self.entry_money.place(x = 200, y = 110)

        self.combobox = ttk.Combobox(self, values=[u'Прибуток', u'Витрати'])
        self.combobox.current(0)
        self.combobox.place(x = 200, y = 80)

        self.btn_cancel = ttk.Button(self, text = "Close", command = self.destroy)
        self.btn_cancel.place(x=300, y=150)

        self.btn_ok = ttk.Button(self, text="Add")
        self.btn_ok.place(x=200, y =150)
        self.btn_ok.bind('<Button-1>', lambda event: self.view.records(self.entry_description.get(),
                                                                       self.entry_money.get(),
                                                                       self.combobox.get()))

        self.grab_set()
        self.focus_set()

class DB:
    def __init__(self):
        self.conn = sqlite3.connect('finance.db')
        self.c = self.conn.cursor()
        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS finance (id integer primary key, desc text, costs text, total real)''')
        self.conn.commit()

    def insert_data(self, desc, costs, total):
        self.c.execute('''INSERT INTO finance(desc, costs, total) VALUES (?, ?, ?)''',
                       (desc, costs, total))
        self.conn.commit()
if __name__ == '__main__':
    root = tk.Tk()
    db = DB()
    app = Main(root)
    root.title('Try')
    root.geometry('650x450+300+200')
    root.resizable(False, False)
    root.mainloop()
